const CHOICES = ["ROCK", "PAPER", "SCISSOR"];

function getComputerChoice() {
    let random = Math.floor(Math.random() * 3);
    return CHOICES[random];
} //Random computer choice


function determineTheWinner(playerChoice, computerChoice) {
    if (playerChoice === computerChoice) {
        return "<p>Draw</p>";
    } //Draw

    if (playerChoice === "PAPER") {
        if (computerChoice === "ROCK") {
            return `Player 1 <br> Win`;
        } else {
            return "Com <br> Win";
        }
    }

    if (playerChoice === "ROCK") {
        if (computerChoice === "SCISSOR") {
            return `Player 1 <br> Win`;
        } else {
            return "Com <br> Win";
        }
    }

    if (playerChoice == "SCISSOR") {
        if (computerChoice === "PAPER") {
            return `Player 1 <br> Win`;
        } else {
            return "Com <br> Win"
        }
    }

}

let getRock = document.getElementById("rock");
let getPaper = document.getElementById("paper");
let getScissor = document.getElementById("scissor");



let theResult = document.getElementById("the-result")

getRock.addEventListener("click", player);
getPaper.addEventListener("click", player);
getScissor.addEventListener("click", player);




function player(event) {
    const getPlayerChoice = event.target.id;
    const playerChoice = getPlayerChoice.toUpperCase();
    const computerChoice = getComputerChoice();


    let optionEfek = `${computerChoice.toLowerCase()}-c`;

    let comChoice = document.getElementById(`${optionEfek}`);

    comChoice.parentElement.classList.add("img-choice");
    event.target.parentElement.classList.add("img-choice");

    // // Efek rotasi gambar
    comChoice.classList.add("com-rotate");
    event.target.classList.add("player-rotate");

    theResult.classList.remove("h2");
    theResult.classList.add("box-win");


    theResult.innerHTML = `${determineTheWinner(playerChoice, computerChoice)}`

    getRock.removeEventListener("click", player);
    getPaper.removeEventListener("click", player);
    getScissor.removeEventListener("click", player);

    console.log(playerChoice)
    console.log("Permainan dimulai...");
    console.log(`Player 1 memilih pilihan: ${playerChoice}`);
    console.log(`Computer memilih pilihan: ${computerChoice}`);
    console.log(`Hasil pertandingan adalah ${determineTheWinner(playerChoice, computerChoice)}`);
}





function restart() {
    document.location.reload()
}



